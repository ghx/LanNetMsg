package cn.guo.app.lan.msg.interfaces;

/**
 *
 * Created by guo on 2015/10/29.
 */
public interface  Callback<T> {
    public void run(String ip,T data);
}
