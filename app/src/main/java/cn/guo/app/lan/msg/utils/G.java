package cn.guo.app.lan.msg.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * 全局变量
 * Created by guo on 2015/10/29.
 */
public class G {
    public static final String MSG_SERVICE_ACTION_NAME = "cn.guo.app.lan.msg.service.MSG_SERVICE";
    public static final String TAG = "cn.guo.app.lan.msg";
    public static void w(String msg){
        Log.w(TAG,msg);
    }
    public static void t(Context context,String msg){
        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
    }
}
